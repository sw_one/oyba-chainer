export const P = {
    SCREEN_W: 640,
    SCREEN_H: 480,
    UNDER_WH: 40,
    GRID_OFFSET_X: 15,
    GRID_OFFSET_Y: 145,
    level: ''
};
export let SCORES = {
    TILES: { 
        Red: 0,
        Green: 0,
        Blue: 0,
    },        
    SCORE: 0
};