import { P } from './params';
import { graphics } from './scenes/ChainerScene';
import { chain } from './EventEmitter';

function drawPoint(x, y) {
    graphics.fillStyle(0x00ff00, 0.5);
    
    const POINT_OFFSET = 10 + 10;
    
    x = x * P.UNDER_WH + P.GRID_OFFSET_X + POINT_OFFSET;
    y = y * P.UNDER_WH + P.GRID_OFFSET_Y + POINT_OFFSET;

    graphics.fillCircle(x, y, 10);
}

function drawLine(x0, y0, x, y) {

    const LINE_OFFSET = 20;
    
    graphics.lineStyle(5, 0x00ff00, 0.5);
    graphics.lineBetween(
        x0 * P.UNDER_WH + P.GRID_OFFSET_X + LINE_OFFSET,
        y0 * P.UNDER_WH + P.GRID_OFFSET_Y + LINE_OFFSET,
        x * P.UNDER_WH + P.GRID_OFFSET_X + LINE_OFFSET,
        y * P.UNDER_WH + P.GRID_OFFSET_Y + LINE_OFFSET
    );

}

export function drawChain() {
    graphics.clear();
    
    let c = '∞:';
    let l = chain.length;

    for(let i = 0; i < l; i++) {
        let x = chain[i].x;
        let y = chain[i].y;
        
        c += `(${x}, ${y})`;
        drawPoint(x, y);

        if(i < l - 1) {
            c+= '->';
            let nx = chain[i + 1].x;
            let ny = chain[i + 1].y;
            drawLine(x, y, nx, ny);
        }
    }

    // console.log(c);
}