import { TileTypes } from './Tile';

export const tileCodes = {
    0: TileTypes.Red,
    1: TileTypes.Green,
    2: TileTypes.Blue
};