// level 1 tiles
export const grid = [
    [2,0,2,2,2],
    [2,0,0,0,2],
    [2,2,2,0,2],
    [2,2,2,2,2]
];

// const COUNT_H = 5;
// const COUNT_V = 4;

export const COUNT_V = grid.length;
export const COUNT_H = grid[0].length;

