import GButton from '../GButton';
import { loadGButtonSrc } from '../GButton';
import { emitter } from '../EventEmitter';
import { P } from '../params';
/*
const Dropbox = require('dropbox').Dropbox;
const dbx = new Dropbox({ accessToken: 'qgXrIbvxFTQAAAAAAABvUvvglx9iOdIggsjhIZJTJp4l33JyTRTIn4VKDgf_wgO9' });
*/

const images = [ 'sky' ];

function getConfig () {
    const p = 'app.conf.json';
        return new Promise((resolve) => {
            fetch(p)
                .then(res => res.json())
                .then(json => { 
                    console.log(json);
                    // this.serverPath = json.serverPath;
                    resolve(json);
                })
        })
}

/*
function dbConnect () {
    // return const name = new type(arguments);
    dbx.filesListFolder({ path: ''})
        .then(response => {
            console.log('dbConnect r->', response);
        })
        .catch(err => console.error('dbConnect e->', err));
}
*/

let sp;
let statusBar;

function setStatus(txt) {
    statusBar.text = txt;
    let t = statusBar.getBounds();
    // console.log(t);
    statusBar.x = P.SCREEN_W / 2 - t.width / 2;
}

function getLevelsData(config) {
    // console.log('config: ', config);
    const url = new URL(config.serverPath);
    sp = config.serverPath;
    // 'http://localhost:8079/oyba-chain-ts/src/server/data.php');
    const q = { q: 'fdb' };
    url.search = new URLSearchParams(q);

    return new Promise((resolve, reject) => { 
        setStatus('Load levels data');
        fetch(url, {
            method: 'GET',
            mode: 'cors'
            })
            .then(response => response.json())
            .then(json => {
                console.log('res->', json);
                resolve(json);
            })
            .catch(err => { 
                setStatus('Data server not responding');
            });
    })
}

function createLevelButtons(ctx, data) {
    // create level buttons
    let buttonW = 120;
    let buttonH = 40;
    let centerW = P.SCREEN_W / 2 - buttonW / 2; // 200


    data.forEach((e, i) => {
        let button = new GButton(ctx, centerW, 50 + 50 * i, buttonW, buttonH, e[1]);
        button.setAction(() => { 
            emitter.emit('event:set-active-level', { data: e, ctx });
            // ctx.scene.start('ChainerScene');
        });
    });
    setStatus('Ready!');
}

class CenterTextBox {
    constructor(ctx, y, text, style = { fontSize: '36px' }) {
        this.ctx = ctx;

        this.style = {
            fontSize: style.fontSize,
            fill: '#ffff00'
        };

        this.text = ctx.add.text(0, y, '', this.style);

        this.setText(text);
    }

    setText(text) {
        this.text.setText(text);
        let metrix = this.text.getBounds();
        this.text.x = P.SCREEN_W / 2 - metrix.width / 2;
    }

}

class MenuScene extends Phaser.Scene {
    constructor() {
        super({
            key: 'MenuScene'
        });
        // this.level = 'level_2';
    }

    preload () {
        images.forEach(i => this.load.image(i, `assets/${i}.png`));
        loadGButtonSrc(this);
        // dbConnect();
    }
    
    create() {
        this.add.image(0, 0, 'sky').setOrigin(0, 0);

        let headerTxt = 'Oyba Chainer Game';
        let header = this.add.text(10, 10, headerTxt, {
            fontSize: '36px',
            fill: '#ffff00'
        });
        let headerSize = header.getBounds();
        header.x = P.SCREEN_W / 2 - headerSize.width / 2;

        statusBar = this.add.text(P.SCREEN_W / 2, P.SCREEN_H - 40, '', {
            fontSize: '20px',
            fill: '#000'
        });
        setStatus('status bar');

        new CenterTextBox(this, 300, 'text on center', { fontSize: '20px' });

        /* this.input.once('pointerdown', function () {
            // this.scene.start('ChainerScene');
        }, this); */

        // create g buttons
        let llb = new GButton(this, 10, 50, 100, 20, 'Ping');
        llb.setAction(() => {
            const url = new URL(sp);
            const q = { q: 'fdb' };
            url.search = new URLSearchParams(q);

            return new Promise((resolve, reject) => { 
                fetch(url, {
                    method: 'GET',
                    mode: 'cors'
                    })
                    .then(response => response.json())
                    .then(json => {
                        console.log('res->', json);
                    })
            })

        });
        llb.underlay.tintFill = false;
        llb.underlay
            .setAlpha(0.8)
            .setTint('0xff00ff');

        let goToMerge = new GButton(this, 10, 80, 100, 20, 'Merger');
        goToMerge.setAction(() => {
            emitter.emit('event:go-to-merge');
            this.scene.start('MergerScene');
        });
        /*
        let b0 = new GButton(this, 200, 100, 120, 40);
        b0.setAction(() => { 
            console.log('Button 0');
            this.scene.start('ChainerScene');
        });
        b0.setText('Button 0');
        */
        
        getConfig()
            .then(j => getLevelsData(j))
            .then(data => createLevelButtons(this, data))
    }

    update() {

    }
}

export default MenuScene;