import GButton, {loadGButtonSrc} from '../GButton';

import {P} from '../params';
import {merger} from './mergerActions/merger';
import {loadLevel} from './loadLevel';
import {drawMergerGrid} from './drawGrid';
import {moveCards} from './mergerActions/moveCards';
import {createBG} from './mergerActions/createBG';
import {createMergerLevel} from './mergerActions/createMergerLevel';

/* const grid = [
    [0,2,0,0,0],
    [2,0,2,0,0],
    [2,2,2,2,0],
    [0,0,0,0,0]
]; */

const images = [ 'underlayB' ];

function moveSomeCards(ctx) {
    return () => {
        let kards = [{
            x: 0,
            y: 0
        }, {
            x: 4,
            y: 3
        }];

        moveCards(ctx, kards);


        let cards1 = [{
            x: 1,
            y: 1
        }];
        moveCards(ctx, cards1);
    };
}

function resetCards(ctx) {
    return () => {
        ctx.cards.forEach(row => row.forEach(card => {
            let x = card.x;
            let y = card.y;
            card.setGridPos(x, y);
        }))
    };
}

class MergerScene extends Phaser.Scene {
    constructor() {
        super({
            key: 'MergerScene'
        });
        this.cards = null;
    }

    preload() {
        images.forEach(i => this.load.image(i, `assets/${i}.png`));
        loadGButtonSrc(this);
    }

    create() {

        let closeButton = new GButton(this, P.SCREEN_W - 40, 10, 30, 30, 'X');
        closeButton.setAction(() => this.scene.start('MenuScene'));

        const LVL_N = `m_level_0.json`;
        loadLevel(LVL_N)
            .then(j => {
                console.log('level->', j);

                createBG(this, j.h, j.v);
                // let gg = this.add.graphics();
                drawMergerGrid(this, j.h, j.v);

                this.cards = createMergerLevel(this, j);
                console.log('cards->', this.cards);
                merger.fillGrid(j.grid);

            });

        const BUT_X = P.SCREEN_W - 100 - 45;

        let runMergerButton = new GButton(this, BUT_X, 10, 100, 40, 'Run Merger');
        runMergerButton.setAction(() => {

            merger.setStartPoint(1, 0);
            merger.run();
            console.log(merger.processed);
        });

        let moveCardsButton = new GButton(this, BUT_X, 10 + 40 + 5, 100, 40, 'Move Cards');
        moveCardsButton.setAction(moveSomeCards(this));


        let resetCardsButton = new GButton(this, BUT_X, 10 + 40 * 2 + 5 * 2, 100, 40, 'Reset Cards');
        resetCardsButton.setAction(resetCards(this));

    }

    update() {

    }
}

export default MergerScene;