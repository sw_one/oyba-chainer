const LEVELS_PATH = 'src/levels/';

export function loadLevel(levelName) {
    return new Promise(resolve => {
        fetch(`${LEVELS_PATH}${levelName}`) //.json
            .then(response => response.json())
            .then(json => {
                // console.log('TCL: loadLevel -> json', json);
                // return json;
                json.v = json.grid.length;
                json.h = json.grid[0].length;
                resolve(json);
            })
    })
}