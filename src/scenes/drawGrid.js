import {P} from '../params';
import {CARD_DIM, MX, MY, padding} from './mergerActions/config';
// import {Panel} from './mergerActions/Panel';
import {COLORS} from './COLORS';


export function drawChainGrid(graphics, h, v) {
    let lx = P.GRID_OFFSET_X;
    let ly = P.GRID_OFFSET_Y;
    let u = P.UNDER_WH;

    drawGrid(graphics, h, v, { offsetX: lx, offsetY: ly, cellWidth: u })
}

export function drawMergerGrid(ctx, h, v) {
    /*
    let graphics = ctx.add.graphics();

    let half = CARD_DIM / 2;


    drawGrid(graphics, h, v,
        { 
            offsetX: (MX - half) - padding, // MX / 2,
            offsetY: (MY - half) - padding, //MY / 2,
            cellWidth: CARD_DIM 
        })
     */

    const offsetX = MX; // (MX - half) - padding;
    const offsetY = MY; // (MY - half) - padding;

    console.log('offs', offsetX, offsetY);

    let drops = [];

    for (let y = 0; y < v; y++) {
        let row = [];
        for (let x = 0; x < h; x++) {

            let x0 = offsetX + CARD_DIM * x;
            let y0 = offsetY + CARD_DIM * y;

            // create zone
            let zone = ctx.add.zone(x0, y0, CARD_DIM, CARD_DIM).setRectangleDropZone(CARD_DIM, CARD_DIM);
            zone.orient = { x, y };

            // show zone grid
            let graphics = ctx.add.graphics();
            graphics.lineStyle(2, COLORS.BLUE);
            graphics.strokeRect(zone.x - zone.input.hitArea.width / 2, zone.y - zone.input.hitArea.height / 2, zone.input.hitArea.width, zone.input.hitArea.height);

            row.push(zone);
        }
        drops.push(row);
    }

    return drops;

}

export function drawGrid(graphics, h, v, { offsetX, offsetY, cellWidth }) {
    
    graphics.fillStyle(0xff0000, .5);
    
    for(let i = 0; i <= h; i++) {
        let x = offsetX + i * cellWidth;
        graphics.fillRect(x, offsetY, 2, cellWidth * v);
    }
    
    for(let i = 0; i <= v; i++) {
        let y = offsetY + i * cellWidth;
        graphics.fillRect(offsetX, y, cellWidth * h, 2);
    }
}