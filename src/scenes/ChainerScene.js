// import { grid, COUNT_V, COUNT_H } from './levels';
import { P } from '../params';
import { GridContainer } from '../GridContainer';
import { SCORES } from '../params';

import GButton from '../GButton';
import { loadLevel } from './loadLevel';
import { createChainerLevel } from './chainerActions/createChainerLevel';
import { drawChainGrid } from './drawGrid';

const images = [ 'sky', 'underlay', 'gemBlue', 'gemRed', 'gemGreen' ];

export let graphics;
// let _grid = [];
export let gridContainer;

let scoreTextObj;

export function updateScore() {
    console.log('update score', SCORES);
    
    let parts = '';
    Object.keys(SCORES.TILES).forEach(t => {
        parts += `${t.charAt(0)}:${SCORES.TILES[t]} `;
    });

    // console.log('parts:', parts);
    
    const scoreText = () => `Score: ${SCORES.SCORE} > ${parts}`;
    scoreTextObj.setText(scoreText());
}

class ChainerScene extends Phaser.Scene {
    constructor() {
        super({
            key: 'ChainerScene'
        });
        // this.level = 'level_2.json';
    }

    preload () {
        images.forEach(i => this.load.image(i, `assets/${i}.png`));
    }

    create() {
        this.add.image(0, 0, 'sky').setOrigin(0, 0);
        
        // let gridMask = createMask(this);
        
        scoreTextObj = this.add.text(10, 10, '', {
            fontSize: '18px',
            fill: '#ffff00'
        });
        updateScore();

        loadLevel(P.level)
            .then(j => { 
                let _grid = createChainerLevel(this, j);
                // console.log('_grid: ->', _grid);

                gridContainer = new GridContainer(_grid);
                gridContainer.print();

                graphics = this.add.graphics();
                drawChainGrid(graphics, j.h, j.v);
            });

        /*
        let exitB = this.add.text(P.SCREEN_W - 20, 10, 'X', {
            fontSize: '18px',
            fill: '#ff0000'
        });

        exitB.setInteractive();
        exitB.on('pointerdown', function () {
            console.log('zzzz');
            this.scene.start('MenuScene');
        }, this);
        */

        let b = new GButton(this, P.SCREEN_W - 40, 10, 30, 30, 'X');
        b.setAction(() => this.scene.start('MenuScene'));
        

        // for(let i = 0; i <= COUNT_V; i++) {
        // graphics.lineBetween(lx, ly, lx + 40, );
        // }
        // let u = P.UNDER_WH;
        /* graphics.fillStyle(0xffff00, .5);
        graphics.fillRect(lx, ly + 0 * u, u, u);
        graphics.fillRect(lx, ly + 1 * u, u, u);
        graphics.fillRect(lx, ly + 2 * u, u, u);
        graphics.fillRect(lx, ly + 3 * u, u, u);
        */

    }

    update() {

    }

    
}

export default ChainerScene;