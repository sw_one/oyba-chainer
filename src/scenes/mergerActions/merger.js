class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    setVal(val) {
        this.val = val;
    }
    setValFromGrid(grid) {
        this.val = grid[this.y][this.x].val;
    }

}

class Merger {
    constructor() {
        this.gridP = [];
        this.GX = 0;
        this.GY = 0;

        this.maxIterations = 14;
        this.iteration = 0;
        this.processed = [];
    
    }

    setStartPoint(x, y) {

        this.processed = [];
        const startPoint = new Point(x, y);
        startPoint.setValFromGrid(this.gridP);
        // console.log(startPoint);

        this.processed.push(startPoint);
    }

    fillGrid(grid) {
        this.gridP = [];
        
        this.GX = grid[0].length - 1;
        this.GY = grid.length - 1;

        for(let j = 0; j <= this.GY; j++) {
            let row = [];
            for(let i = 0; i <= this.GX; i++) {
                let po = new Point(i, j);
                let v = grid[j][i];
                po.setVal(v);
                row.push(po);
            }
            this.gridP.push(row);
        }
        // console.log(this.gridP);
    }

    processCell(cell, x, y) {
        const link = this.gridP[y][x];
        const v = link.val;
        let res = null;
    
        if(cell.x !== x || cell.y !== y) {
            // console.log(`->(${x},${y}):${v}`);
            if(v === cell.val) {
                res = link;
            }
        }
    
        return res;
    }
    
    getBorders(cell) {
        const x = cell.x;
        const y = cell.y;
    
        const minX = Math.max(x - 1, 0);
        const maxX = Math.min(x + 1, this.GX);
    
        const minY = Math.max(y - 1, 0);
        const maxY = Math.min(y + 1, this.GY);
    
        return [ minX, maxX, minY, maxY ];
    }

    checkIn(cell) {
        let found = false;
    
        this.processed.forEach(c => {
            let isX = cell.x === c.x;
            let isY = cell.y === c.y;
    
            if(isX && isY) found = true;
        });
    
        return found;
    }

    checkCellsInRadius(cell) {
        const [ minX, maxX, minY, maxY ] = this.getBorders(cell);
    
        for(let j = minY; j <= maxY; j++) {
            for(let i = minX; i <= maxX; i++) {
    
                let p = this.processCell(cell, i, j);
                if (p) {
                    let isIn = this.checkIn(p);
                    if(!isIn) {
                        this.processed.push(p);
                    }
                }
    
            }
        }
    }
    
    run(from = 0) {
        
        let pl = this.processed.length;

        for (let cc = from; cc < pl; cc++) {
            this.checkCellsInRadius(this.processed[cc]);
        }

        if (this.processed.length > pl && this.iteration <= this.maxIterations) {
            // console.log('start new');
            this.iteration++;
            this.run(this.processed.length - 2);
        } else {
            // console.log(' finished on iteration ' + this.iteration);
            // console.timeEnd('run');
            // console.log(this.processed);

            return this.processed;
        }
    }
}

let merger = new Merger();

export { merger };