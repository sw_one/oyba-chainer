import {COLORS} from '../COLORS';
import {CARD_DIM, MX, MY} from './config';

function getDataBColor(v) {
    return (BIND_COLOR_DATA[v]) ? BIND_COLOR_DATA[v] : COLORS.BLACK;
}

const BIND_COLOR_DATA = {
    0: COLORS.WHITE,
    1: COLORS.RED,
    2: COLORS.GREEN,
    3: COLORS.BLUE
};

export class Panel {
    constructor(ctx, x, y, data) {

        this.underlay = ctx.add.sprite(0, 0, 'underlayB'); //.setOrigin(0, 0);
        this.underlay.id = 'underlay';
        // this.underlay.setScale(2);
        // this.underlay.setOrigin(2);

        this.g = ctx.add.graphics();

        this.g.fillStyle(0xffffff, 0.9);
        // this.g.fillRect(8, 8, 24, 24);
        this.g.fillRect(-12, -12, 24, 24);
        this.g.id = 'icon';

        this.text = ctx.add.text(-20, -20, data, { fontSize: '34px', fill: '#000000' });
        this.text.id = 'text';
        this.text.setPadding(10,5,0,0);

        this.container = ctx.add.container(ctx, 0, 0);
        
        this.container.setSize(this.underlay.width, this.underlay.height);
        this.container.key = `CardCont${x}${y}`;
        this.container.orient = { x, y };
        this.container.setInteractive();

        ctx.input.setDraggable(this.container);
        
        this.container.add(this.underlay);
        this.container.add(this.g);
        this.container.add(this.text);

        this.setGridPos(x, y);
        this.setData(data); // = data;

        this.isDragged = false;

        this.id = `CardCont${x}${y}`;

        
        this.container.on('pointerdown', (pointer, gameObject) => {
            console.log('Tap->', gameObject, this);
            // gameObject.setTint(0xff0000);
        });

        ctx.input.on('dragstart', function (pointer, gameObject) {
            if(!this.isDragged) {
                this.isDragged = true;
                console.log('dragstart', gameObject.orient);
            }
        });
        ctx.input.on('drag', function (pointer, gameObject, dragX, dragY) {
            gameObject.x = dragX;
            gameObject.y = dragY;
        });
        /*
        ctx.input.on('dragend', function (pointer, gameObject, dropped) {
            // gameObject.clearTint();
            if(this.isDragged) {
                console.log('dragend', gameObject.orient);
                // this.setGridPos(this.x, this.y);
            }
            this.isDragged = false;
        });
         */
        ctx.input.on('drop', function (pointer, gameObject, dropZone) {
            if(this.isDragged) {
                console.log('drop', gameObject.orient, dropZone.orient);
            }
            this.isDragged = false;

        });
        

    }
    setPos(x, y) {
        this.container.getChildren().forEach(k => {
            k.x = x;
            k.y = y;
        });
    }
    setGridPos(x, y) {
        this.x = x;
        this.y = y;

        // this.setPos(this.x * CARD_DIM + MX, this.y * CARD_DIM + MY);
        this.container.setPosition(this.x * CARD_DIM + MX, this.y * CARD_DIM + MY);
    }

    setText(v) {
        this.text.text = v;
    }
    setData(v) {
        this.data = v;
        this.underlay.setTint(getDataBColor(v));
    }
}