import {CARD_DIM, MX, MY, padding} from './config';

export function createBG(ctx, h, v) {
    ctx.graphics = ctx.add.graphics();
    ctx.graphics.fillStyle(0xccffcc, 1);
    // ctx.graphics.fillRect((MX / 2) - 3, (MY / 2) - 3, CARD_DIM * h + 3, CARD_DIM * v + 3);
    let half = CARD_DIM / 2;
    ctx.graphics.fillRect((MX - half) - padding, (MY - half) - padding, CARD_DIM * h + padding * 2, CARD_DIM * v + padding * 2);

}