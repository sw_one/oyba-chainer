import {Panel} from './Panel';

export function createMergerLevel(ctx, levelJSON) {
    const grid = levelJSON.grid;
    const COUNT_V = levelJSON.v;
    const COUNT_H = levelJSON.h;

    let panels = [];
    // let gridMask = createMask(ctx, COUNT_H, COUNT_V);

    for (let y = 0; y < COUNT_V; y++) {
        let row = [];
        for (let x = 0; x < COUNT_H; x++) {

            let data = grid[y][x];
            let tile = new Panel(ctx, x, y, data);

            // tile.setMask(gridMask);
            tile.x = x;
            tile.y = y;
            row.push(tile);
        }
        panels.push(row);
    }

    return panels;
}