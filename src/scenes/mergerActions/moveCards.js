export function moveCards(ctx, cards) {
    let twc = [];
    let x = 2;
    let y = 2;

    const duration = 300;

    console.log(ctx);

    cards.forEach(card => {
        twc = twc.concat(ctx.cards[card.y][card.x].container)
        //.getChildren().map(c => { return c }));
    });

    // console.log('twc: ', twc);

    ctx.tweens.add({
        targets: twc,
        /*
        props: {
            x: { value: x, duration, ease: 'Linear' },
            y: { value: y, duration, ease: 'Linear' }
        },
        */
        x,
        y,
        duration,
        ease: 'Linear',
        onComplete: () => {
            console.log('Move cards finished');
        }
    });
}