import { P } from '../../params';
import { Tile } from '../../Tile';
import { tileCodes } from '../../tileCodes';
import { tiles } from '../../main';

function createMask(ctx, h, v) {
    let shape = ctx.make.graphics();
    shape.beginPath();

    let len_x = P.UNDER_WH * h;
    let len_y = P.UNDER_WH * v;
    let mask_margin = 3;

    shape.fillRect(P.GRID_OFFSET_X, P.GRID_OFFSET_Y, len_x + mask_margin, len_y + mask_margin);
    let mask = shape.createGeometryMask();

    return mask;
}

export function createChainerLevel(ctx, levelJSON) {
    const grid = levelJSON.grid;
    const COUNT_V = levelJSON.v;
    const COUNT_H = levelJSON.h;
    
    let gridMask = createMask(ctx, COUNT_H, COUNT_V);
    let _grid = [];
    
    for(let y = 0; y < COUNT_V; y++) {
        _grid.push([]);
        for (let x = 0; x < COUNT_H; x++) {

            let _t = grid[y][x];
            let tile = new Tile(ctx, x, y, tileCodes[_t]);

            // tile.setMask(gridMask);
            tile.x = x;
            tile.y = y;
            tile.setText();

            tiles.push(tile);
            _grid[y].push(tileCodes[_t].charAt(0));
        }
    }

    return _grid;
    
}