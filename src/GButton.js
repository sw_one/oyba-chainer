// const winParts = [ 'CornerTL', 'FrameT', 'Underlay' ];
// const winSet = 'dbThin';

const winParts = [ 'CornerTL', 'FrameT', 'Underlay' ];
const winSetD = [ 'dbOldBlue', 'dbBlue', 'dbGray', 'dbThin' ];
const winSet = winSetD[3];

export function loadGButtonSrc(ctx) {
    winParts.forEach(i => ctx.load.image(i, `assets/winman/${winSet}/${i}.png`));
}

class GButton {
    constructor(ctx, x, y, width, height, btext = 'Button') {
        this.w_x = x;
        this.w_y = y;
        this.w_w = width;
        this.w_h = height;
        // console.log(ctx);
        /* let cornerRes = ctx.textures.get('CornerTL').source[0];
        let cw = cornerRes.width;
        let ch = cornerRes.height;
        console.log(cornerRes, cw, ch); */

        let u = ctx.add.image(x, y, 'Underlay').setOrigin(0, 0);
        u.displayWidth = width;
        u.displayHeight = height;
        u.setInteractive();

        let tf = ctx.add.image(x, y, 'FrameT').setOrigin(0, 0);
        tf.displayWidth = width;

        let df = ctx.add.image(x + width, y + height, 'FrameT').setOrigin(0, 0);
        df.displayWidth = width;
        df.angle = 180;

        let lf = ctx.add.image(x, y + height, 'FrameT').setOrigin(0, 0);
        // noinspection JSSuspiciousNameCombination
        lf.displayWidth = height;
        lf.angle = -90;

        let rf = ctx.add.image(x + width, y, 'FrameT').setOrigin(0, 0);
        // noinspection JSSuspiciousNameCombination
        rf.displayWidth = height;
        rf.angle = 90;

        let lu = ctx.add.image(x, y, 'CornerTL').setOrigin(0, 0);
        let ru = ctx.add.image(x + width, y, 'CornerTL').setOrigin(0, 0);
        ru.angle = 90;
        let ld = ctx.add.image(x, y + height, 'CornerTL').setOrigin(0, 0);
        ld.angle = -90;
        let rd = ctx.add.image(x + width, y + height, 'CornerTL').setOrigin(0, 0);
        rd.angle = 180;


        this.text = ctx.add.text(x, 0, '', {
            fontSize: '14px',
            // stroke: '#fff000',
            // strokeThickness: 1,
            fill: '#fff000',
            // fontFamily: 'Tahoma'
        });

        this.setText(btext);

        let container = ctx.add.group();

        container.add(u);

        container.add(tf);
        container.add(df);
        container.add(lf);
        container.add(rf);

        container.add(lu);
        container.add(ru);
        container.add(ld);
        container.add(rd);

        container.add(this.text);

        u.on('pointerdown', () => {
            console.log('Tap it!');
            if(this.action) this.action();
        });

        this.underlay = u;
    }
    setAction(fun) {
        this.action = fun;
    }
    setText(txt = '') {
        this.text.setText(txt);

        let tm = this.text.getBounds();
        // console.log('tm: ', tm);

        this.text.x = this.w_x + ((this.w_w / 2) - (tm.width / 2));
        this.text.y = this.w_y + ((this.w_h / 2) - (tm.height / 2));
    }
}

export default GButton;