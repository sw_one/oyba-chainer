import { drawChain } from './drawChain';
import { chain, chainType, startX, startY, addToChain, setCurrentPoint } from './EventEmitter';

function checkInChain(x, y) {
    let pos = -1;
    chain.forEach((cell, i) => {
        if(cell.x === x && cell.y === y) pos = i;
    });
    return pos;
}

export function checkCell(data) {
    //check distance
    let distance = Math.sqrt((startX - data.x)**2 + (startY - data.y)**2);
    
    if (data.type === chainType && distance < 2) {
        
        let pos = checkInChain(data.x, data.y);
        
        if(pos === -1) {
            data.ctx.hlOn();
            addToChain(data);
            drawChain();
            setCurrentPoint(data.x, data.y);
        } else {
            let prevPos = chain.length - 2;
            if(pos === prevPos) {
                // console.log('is previous', chain[chain.length - 1]);
                chain[chain.length - 1].ctx.hlOff();
                chain.pop();
                drawChain();
                setCurrentPoint(data.x, data.y);
            }
        }
    }
}