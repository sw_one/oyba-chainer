import 'phaser';
// import BootScene from './scenes/BootScene';
// import GameScene from './scenes/GameScene';
// import TitleScene from './scenes/TitleScene';
import MenuScene from './scenes/MenuScene';
import ChainerScene from './scenes/ChainerScene';
import MergerScene from './scenes/MergerScene';
import { P } from './params';
import { COUNT_V, COUNT_H } from './levels';
import * as Phaser from "phaser";



export let tiles = [];

export let V = COUNT_V;
export let H = COUNT_H;

let config = {
    type: Phaser.AUTO,
    width: P.SCREEN_W,
    height: P.SCREEN_H,
    parent: 'phaser-example',
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 300 },
            debug: false
        }
    },
    scene: [
        MergerScene,
        MenuScene,
        ChainerScene,
        
    ] /* {
        preload: preload,
        create: create,
        update: update
    } */
};

export let game = new Phaser.Game(config);