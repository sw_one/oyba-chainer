export class GridContainer {
    constructor(grid) {
        this.grid = grid;
        this.ver = grid.length;
        this.hor = grid[0].length;

        this.dot = 'x';
    }
    print() {
        for (let j = 0; j < this.ver; j++) {
            console.log(this.grid[j]);
        }
    }
    makeDots(dots = []) {
        dots.forEach(dot => this.grid[dot.y][dot.x] = this.dot);
    }
    getCol(number) {
        let col = [];
        for (let i = 0; i < this.ver; i++) {
            col.push(this.grid[i][number]);
        }
        return col;
    }
    setCol(number, col) {
        for (let i = 0; i < this.ver; i++) {
            this.grid[i][number] = col[i];
        }
    }
    countDots() {
        let dots = [];
        for (let i = 0; i < this.hor; i++) {
            let dot = this.getCol(i);
            let n = 0;
            dot.forEach(c => {
                if (c === this.dot) n++;
            });
            dots.push(n);
        }
        return dots;
    }
    setVal(x, y, val) {
        // console.log('set->', x, y, val);
        this.grid[y][x] = val.charAt(0);
    }
    setDot(x , y) {
        this.setVal(x ,y, this.dot);
    }
    proceedDots() {
        let dots = this.countDots();
        dots.forEach((d, i) => this.shiftCol(i, d));
    }
    shiftCol(num, shift, val = 3) {
        let col = this.getCol(num);

        for (let zz = 0; zz < shift; zz++) {
            col.pop();
            col.unshift(val);
        }

        this.setCol(num, col);
    }
    checkDotsBelow(x, y) {
        let dots = 0;
        let col = this.getCol(x).slice(y + 1);
        col.forEach(c => {
            if (c === this.dot) dots++;
        });
        return dots;
    }
    shiftCellDown(x, y, bias) {
        let val = this.grid[y][x];
        this.grid[y][x] = this.dot;
        this.grid[y + bias][x] = val;
    }
    checkRow(n) {
        let shifts = [];
        this.grid[n].forEach((p, i) => {
            if (p !== this.dot) {
                let dots = this.checkDotsBelow(i, n);
                if (dots) {
                    this.shiftCellDown(i, n, dots);
                    shifts.push({ x: i, y: n, shift: dots })
                }
            }
        });
        // console.log('check row', JSON.stringify(shifts));
        return shifts;
    }
    checkRows() {
        let shifts = [];
        let lr = this.ver - 2;
        for (let r = lr; r >= 0; r--) {
            let shift = this.checkRow(r);
            // console.log('->', shift);
            shifts = shifts.concat(shift);
        }
        return shifts;
    }
}