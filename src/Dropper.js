const SPEED = 4; // less - faster
const BIAS = 40; // sprite 40x40

function createSteps(bias, speed) {
    let parts = Math.floor(bias / speed);
    let rem = bias % speed;

    let steps = [];
    for (let i = 0; i < speed; i++) {
        steps.push(parts);
    }
    steps.push(rem);

    return steps;
}

export class Dropper {
    constructor(tile) {
        this.tile = tile;
        this.steps = createSteps(BIAS, SPEED);
        this.currentStep = 0;
        
        this.tiles = [];

        this.callback = null;
    }
    drop() {
        this.tile.container.getChildren().forEach(c => c.y += this.steps[this.currentStep]);
        this.currentStep++;
        if(this.currentStep < this.steps.length) {
            requestAnimationFrame(this.drop.bind(this));
        } else {
            this.tile = null;
            this.currentStep = 0;
        }
    }
    /*
    tileObj = {
        tile,
        currentStep,
        pos
    }
     */
    addTile(name, tile, pos = 1) {
        this.tiles.push({
            tile: tile ,
            currentStep: 0,
            pos: pos
        });
    }
    processTile (tileObj) {
        // console.log(tileObj);
        if (tileObj.pos !== 0) {
            
            // console.log(`process ${tileObj.tile.x} ${tileObj.tile.y}`);
            tileObj.tile.container.getChildren().forEach(c => c.y += this.steps[tileObj.currentStep]);
            tileObj.currentStep++;
            if (tileObj.currentStep < this.steps.length) {
                // cont = true;
            } else {
                // console.log(`processed ${tileObj.tile.x} ${tileObj.tile.y}`);
                tileObj.pos--;
                tileObj.currentStep = 0;
                tileObj.tile.y += 1;
                tileObj.tile.setText();
            }
            
            
            /*
            tileObj.tile.container.getChildren().forEach(c => c.y += 40);
            
            tileObj.pos--;
            tileObj.tile.y += 1;
            tileObj.tile.setText(); */
        } 
    }
    dropAll() {

            this.tiles.forEach(tile => this.processTile(tile));

            const reducer = (acc, cv) => acc + cv.pos;
            let pr = this.tiles.reduce(reducer, 0);

            if (!!pr) { 
                requestAnimationFrame(this.dropAll.bind(this)); 
            } else { 
                this.clear();
            } 
        
    }
    clear() {
        // this.tiles.forEach(t => t.tile.hlOff());
        this.tiles = [];
        console.log('Clear', this.tiles);
        if (this.callback) this.callback();
    }
}