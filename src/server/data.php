<?php

header('Access-Control-Allow-Origin: *');
header("Content-type: application/json");

if (!empty($_GET["q"])) {
    $qKey = $_GET["q"];

    if ($qKey === "test") {
        echo "{ \"response\": 0 }";

        /* echo __DIR__;
        echo get_include_path();

        $files = scandir("../".__DIR__);

        print_r($files);
        */
    }

    if ($qKey === "fdb") {
        $dir = "../levels";
        $filelist = array();
        
        if ($handle = opendir($dir)) {
            while ($entry = readdir($handle)) {
                if (strpos($entry, ".json") !== false) {
                    
                    $content = json_decode(file_get_contents($dir."/".$entry), true);
                    // print_r($content);

                    $filelist[] = [ $entry, $content['name']];
                    
                }
            }
            closedir($handle);
        }
        // print_r($filelist);
        echo json_encode($filelist);
    }

}