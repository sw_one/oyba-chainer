// import { V, H, tiles } from './main';
import { gridContainer } from './scenes/ChainerScene';
import { dropper, findTile } from './EventEmitter';
import { Dropper } from './Dropper';
import { emitter } from './EventEmitter';


let nDropper = new Dropper();

export function processChain(chain) {
    // console.log(chain.length);
    let nd = [];
    let scores = {};

    chain.forEach(cell => {
        // console.log(cell);
        let x = cell.x;
        let y = cell.y;

        // cell.ctx.destroy();
        cell.ctx.setCell(x, -1);
        cell.ctx.y = -1;
        cell.y = -1;
        cell.ctx.setText();
        
        //scores
        let type = cell.type;
        if(!scores[type]) scores[type] = 0;
        scores[type]+= 1;

        if(!nd[x]) nd[x] = [];
        nd[x].push(cell);

        gridContainer.setDot(x, y);
    });

    emitter.emit('event:process-chain', scores);

    nd.forEach((r, i) => {
        r.forEach((e, j) => {
            nDropper.addTile(i + j, e.ctx, j + 1);
        })
    });

    const onDropEnd =  function () {
        // console.log('new dropped');
        // update grid
        nd.forEach((r, i) => {
            r.forEach((e, j) => {
                gridContainer.setVal(i, j, e.type);
            })
        })
    };
    nDropper.callback = onDropEnd;
    nDropper.dropAll();

    let shifts = gridContainer.checkRows();
    console.log(JSON.stringify(shifts));

    shifts.forEach((entry, i) => {
        let t = findTile(entry.x, entry.y);
        dropper.addTile(i, t, entry.shift);
    });

    dropper.dropAll();
}