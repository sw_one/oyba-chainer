import { app } from './app';
import { Dropper } from './Dropper';
import { tiles } from './main';

import { drawChain } from './drawChain';
import { checkCell } from './checkCell';
import { processChain } from './processChain';

import { SCORES, P } from './params';
import { updateScore } from './scenes/ChainerScene';

class EventEmitter {
    constructor() {
        this.events = {};
    }

    subscribe(eventName, fn) {
        if (!this.events[eventName]) {
            this.events[eventName] = [];
        }

        this.events[eventName].push(fn);

        return () => {
            this.events[eventName] = this.events[eventName].filter(eventFn => fn !== eventFn);
        }
    }

    emit(eventName, data) {
        const event = this.events[eventName];
        if (event) {
            event.forEach(fn => {
                fn.call(null, data);
            });
        }
    }

}

export let emitter = new EventEmitter();

export const Events = {
    TAP: 'event:tap',
    OVER: 'event:over',
    OUT: 'event:out',
    UP: 'event:up'
};

export let lock = false;
export let chainType = '';
export let chain = [];
export let startX = -1;
export let startY = -1;

let setLock = (v) => {
    lock = v;
    app.lock = lock;
};

let setChainType = (t = '') => {
    chainType = t;
    app.chainType = t;
};


export function findTile(x, y) {
    let tile = null;

    tiles.forEach(t => { if(t.x === x && t.y === y) tile = t; });

    return tile;
}

export let addToChain = (tile) => {
    if(tile) {
        chain.push(tile);
    } else {
        chain = []
    }
    app.chain = chain;
};

export const setCurrentPoint = (x = -1, y = -1) => {
    startX = x;
    app.x = x;
    startY = y;
    app.y = y;
};

export let dropper = new Dropper();

emitter.subscribe(Events.TAP, data => {
    // console.log(data);
    setLock(true);
    setChainType(data.type);
    // app.lock = lock;
    console.log('emit->tap', data, lock, data.ctx.underlay);
    data.ctx.hlOn();
    addToChain(data);

    let x = data.x;
    let y = data.y;

    setCurrentPoint(x, y);
    drawChain();

    // data.ctx.drop();
    // dropper.tile = data.ctx;
    // dropper.drop();

    // console.log('emit->up', data);
    /* setLock(false);
    setChainType();
    chain.forEach(tile => tile.ctx.hlOff());
    addToChain(); */
});

emitter.subscribe(Events.OVER, data => {
    if (!lock) return;
    // console.log('emit->over', data);
    checkCell(data);

});

emitter.subscribe(Events.OUT, data => {
    // console.log('emit->out', data);
});

emitter.subscribe(Events.UP, data => {
    // console.log('emit->up', data);
    
    if(chain.length >= 3) {
        console.log('Process chain', chain.length);
        processChain(chain);
    }
    
    setLock(false);
    setChainType();
    chain.forEach(tile => tile.ctx.hlOff());
    addToChain();
    setCurrentPoint();
    drawChain();
});

emitter.subscribe('event:dra', data => {
    console.log('dra');
    dropper.addTile('a', tiles[19]);
    dropper.addTile('b', tiles[18], 2);
    dropper.addTile('c', tiles[17]);
    dropper.dropAll();
});

emitter.subscribe('event:fill-chain', data => {
    console.log('fill-chain');
    let k;
    switch(data) {
        case(1):
            k = [
                [0 , 1],
                [1,  2],
                [2,  2],
                [3,  3],
            ];
        break;
        case(2):
            k = [
                [0 , 1],
                [0 , 2],
                [0 , 3],
                [1 , 3],
                [2 , 3],
            ];
        break;
        case(3):
            k = [
                [1, 2],
                [2, 2],
                [1, 3],
                [2, 3],
            ];
            break;
    }
    
    setChainType('Blue');

    k.forEach(c => { 
        let tile = findTile(c[0], c[1]);
        // console.log(tile);
        tile.hlOn();
        let to = {ctx: tile, type: tile.type, x: tile.x, y: tile. y};
        addToChain(to);
        // console.log(to);
        // drawChain();
    });

    processChain(chain);
    
    setChainType();
    chain.forEach(tile => tile.ctx.hlOff());
    addToChain();
    // drawChain();

});

emitter.subscribe('event:process-chain', data => { 
    console.log('e:p-c', data);

    Object.keys(data).forEach(key => { 
        console.log(key, data[key]);
        SCORES.TILES[key] += data[key];
        SCORES.SCORE += data[key];
    }); 
    updateScore();
});

emitter.subscribe('event:set-active-level', (data) => {
    console.log('event:set-active-level', data);
    P.level = data.data[0];
    console.log(P);
    data.ctx.scene.start('ChainerScene');
});
// let unsubscribe = emitter.subscribe('event:name-changed', data => console.log(data));
// unsubscribe();