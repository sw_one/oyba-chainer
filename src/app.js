import { lock, chainType, emitter } from './EventEmitter';
import { tiles } from "./main";
import { gridContainer } from './scenes/ChainerScene';
import { SCORES } from './params';


let playField = [];
const width = 8;
const height = 7;

for (let y = 0; y < width; y++) {
    let row = [];
    for (let x= 0; x < height; x++) {
       row.push([x, y, 0]);
    }
    playField.push(row);
}

// console.log(playField);

let palette = [];
for(let p = 0; p < 8; p++) {
    palette.push(p);
}

export let app = new Vue({
    el: '#app',
    data: {
        // message: 'Привет, Vue!',
        lock: lock,
        chainType: chainType,
        chain: [],
        x: -1,
        y: -1,
        // gridContainer: gridContainer,
        scores: SCORES,
        playField,
        palette,
        active: 3
    },
    template: `
        <div>
            {{ x }} : {{ y }}<br>
            Lock:{{ lock }}<br>
            chainType:{{ chainType }}<br>
            chain:{{ refineChain() }}<br>
            <button @click="drA()">Drop All</button>
            <button @click="print()">Print</button><br>
            <button @click="tiles()">Tiles</button><br>
            <button @click="fs(1)">chain 1</button>
            <button @click="fs(2)">chain 2</button>
            <button @click="fs(3)">chain 3</button>
            <button @click="printScoresZ()">scores</button><br>
            <span>{{ printScores }}</span>
            active: {{ active }}
            <hr>
            <div>
                <div class="row">Play Field:</div>
                <div v-for="(row, i) in playField" :key="i" class="row">
                    <div v-for="(cell, j) in row" :key="j" class="cell" @click="tapCell"
                     :data-x="i"
                     :data-y="j"
                     :data-val="cell[2]"
                     draggable
                     droppable
                     @dragstart="ods"
                     @dragend="ode"
                     @dragenter="oden"
                     @dragover="odo"
                     @drop="odd"
                    >
                        <!--{{cell[1]}}:{{cell[0]}}-->
                        <br>{{cell[2]}}
                    </div>
                </div>
            </div>
            <div>
                <div class="row">Palette:</div><button @click="printField">field</button>
                <div class="row">
                    <div v-for="(cell, i) in palette" :key="i" 
                        :class="calcClass(i)"
                        :data-val="i"
                        @click="setActive"
                    >
                        {{ cell }}
                    </div>
                </div>
            </div>
        </div>
    `,
    methods: {
        printField() {
            console.table(JSON.srtingify(this.playField));
        },
        oden(e) {
            e.preventDefault();
            return true;
        },
        odo(e) {
            e.preventDefault();
        },
        ods(e) {
            console.log('dragstart', e, e.target.dataset);
            e.dataTransfer.effectAllowed = 'move';
            e.dataTransfer.setDragImage(e.target,1,1);

            // e.dataTransfer.setData("Text", ev.target.getAttribute('id'));

            return true;
        },
        ode(e) {
            // console.log('dragend', e, e.target.dataset);
        },
        odd(e) {
            console.log('drop', e, e.target.dataset);
        },
        calcClass(i) {
            let isActive = (i === this.active) ? 'active' : '';
            return `cell ${isActive} green`;
        },
        setActive(e) {
            let data = e.target.dataset;
            console.log(`Tap pall->(${data.val}), ${typeof data.val}`);
            this.active = parseInt(data.val);
        },
        tapCell(e) {
            let data = e.target.dataset;
            let x = data.x;
            let y = data.y;

            console.log(`Tap->(${x},${y}) = ${data.val}`);
            this.playField[x].splice(y, 1, [y, x, this.active]);
        },
        refineChain() {
            return this.chain.map(tile => ({ type: tile.type.charAt(0), x: tile.x, y: tile.y }))
        },
        drA() {
            emitter.emit('event:dra');
        },
        print() {
            gridContainer.print()
        },
        tiles() {
            console.log(tiles);
        },
        fs(n) {
            emitter.emit('event:fill-chain', n);
        },
        printScoresZ() {
            return JSON.stringify(SCORES);
        }
    },
    computed: {
        printScores: function() {
            return JSON.stringify(this.scores);
        }
    }
});