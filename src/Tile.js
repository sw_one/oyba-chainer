import { P } from './params';
import { emitter } from './EventEmitter';
import { Events } from './EventEmitter';

export const TileTypes = {
    Red: 'Red',
    Green: 'Green',
    Blue: 'Blue'
};



let margins = {
    // underlay: { x: 0, y: 0 },
    gem: { x: 0.4, y: 0.4 },
    text: { x: 7, y: 12 }
};

export class Tile {
    constructor(ctx, x, y, type = TileTypes.Blue) {

        // this.ctx = ctx;
        // this.xx = 0;
        this.type = type;
        
        this.underlay = ctx.add.sprite(0, 0, 'underlay').setOrigin(0, 0);
        this.underlay.id = 'underlay';

        this.gem = ctx.add.image(0, 0, `gem${type}`).setOrigin(0, 0);
        this.gem.id = 'gem';
        
        this.text = ctx.add.text(0, 0, '0', { fontSize: '14px', fill: '#fff000' });
        this.text.id = 'text';
        
        this.container = ctx.add.group();
        this.container.add(this.underlay);
        this.container.add(this.gem);
        this.container.add(this.text);

        this.setCell(x, y);
        
        this.underlay.setInteractive();

        this.addEvents();

    }

    addEvents() {
        this.underlay.on('pointerdown', () => {
            // console.log('Tap!', this);
            // this.hlOn();
            emitter.emit(Events.TAP, this.data())
        });

        this.underlay.on('pointerover', () => {
            // console.log('->');
            emitter.emit(Events.OVER, this.data())
        });
        
        this.underlay.on('pointerout', () => {
            /* this.hlOff();
            emitter.emit(Events.OUT, this.data()); */
        });
        
        this.underlay.on('pointerup', () => {
            //  this.hlOff();
            emitter.emit(Events.UP, this.data())
        });
    }

    setPos(x, y) {
        this.container.getChildren().forEach(c => {
            
            let mx = 0;
            let my = 0;
            let m = margins[c.id];
            if(m) { mx = m.x; my = m.y; }

            c.x = x + mx;
            c.y = y + my;
        });
    }

    setCell(x, y) {
        let posX = x * P.UNDER_WH + P.GRID_OFFSET_X;
        let posY = y * P.UNDER_WH + P.GRID_OFFSET_Y;
        
        this.setPos(posX, posY);
    }

    setMask(mask) {
        this.underlay.setMask(mask);
        this.gem.setMask(mask);
        this.text.setMask(mask);
    }

    setText() {
        let text = `${this.x}:${this.y}`;
        // console.log(this.text, 'text->', text);
        // this.text.setText(text);
        this.text.text = text;
    }
    
    hlOn() {
        this.underlay.setTint(0xff0000);
    }

    hlOff() {
        this.underlay.clearTint();
    }

    hlG() {
        this.underlay.setTint(0x00ff00);
    }

    data() {
        return { ctx: this, type: this.type, x: this.x , y: this.y }
    }
    hide() {
        // this.underlay
    }
    destroy() {
        this.underlay.destroy();
        this.gem.destroy();
        this.text.setVisible();
    }
}

